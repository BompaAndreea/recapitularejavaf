package com.company;

public class CastExample {
    public static void main(String[] args) {
        Parent p= new Child();
        p.name="mama";
        //Can't access id
//        p.id=1;

        p.parentMethod();


        Child child= (Child) p;
        child.id=1;
        child.name="tata";
        child.parentMethod();
    }
}
