package com.company.designPatterns;

public class StartAlarmCommand implements Command{
    MotionAlarm motionAlarm;

    public StartAlarmCommand(MotionAlarm motionAlarm) {
        this.motionAlarm = motionAlarm;
    }

    @Override
    public void execute() {
        motionAlarm.startAlarm();

    }
}
