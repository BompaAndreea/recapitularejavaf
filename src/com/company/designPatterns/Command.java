package com.company.designPatterns;

public interface Command {
    public void execute ();

}
