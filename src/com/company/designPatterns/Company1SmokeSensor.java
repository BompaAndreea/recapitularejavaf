package com.company.designPatterns;

public class Company1SmokeSensor extends SmokeSensor implements Observer{
    public Company1SmokeSensor(int volume) {
        super(volume);
    }

    private int newVolume;

    public int getNewVolume() {
        return newVolume;
    }

    public void setNewVolume(int newVolume) {
        this.newVolume = newVolume;
    }

    @Override
    public void detect() {
        if (newVolume> super.getVolume()){
            System.out.println("Smoke detected!");
            SmokeNotification smokeNotification = new SmokeNotification(newVolume - super.getVolume());
            smokeNotification.sendNotification(new FacebookStrategy());
            smokeNotification.sendNotification(new SmsStrategy());
            HomeController homeController= new HomeController();
            homeController.setCommand(new TurnOnSprinklerCommand(new Sprinkler()));
            homeController.buttonPressed();
        }
    }
}
