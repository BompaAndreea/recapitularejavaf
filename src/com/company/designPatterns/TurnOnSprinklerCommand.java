package com.company.designPatterns;

public class TurnOnSprinklerCommand implements Command{
    Sprinkler sprinkler;

    public TurnOnSprinklerCommand(Sprinkler sprinkler) {
        this.sprinkler = sprinkler;
    }

    @Override
    public void execute() {
      sprinkler.turnON();

    }
}
