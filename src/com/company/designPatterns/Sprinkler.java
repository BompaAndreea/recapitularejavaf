package com.company.designPatterns;

public class Sprinkler {

    public void turnON(){
        System.out.println("Sprinkler is turned on!");
    }
    public void turnOFF(){
        System.out.println("Sprinkler is now turned off!");
    }
}
