package com.company.designPatterns;

public class StopAlarmCommand implements Command {
    MotionAlarm motionAlarm;

    public StopAlarmCommand(MotionAlarm motionAlarm) {
        this.motionAlarm= motionAlarm;

    }

    @Override
    public void execute() {
        motionAlarm.stopAlarm();

    }
}
