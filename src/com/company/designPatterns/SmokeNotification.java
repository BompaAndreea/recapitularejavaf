package com.company.designPatterns;

public class SmokeNotification extends Notification {

    private int details;

    public SmokeNotification(int details) {
        this.details = details;
    }

    @Override
    public String getMessage() {
        return "smoke detected at " + details;
    }
}
