package com.company.designPatterns;

public class HouseApp {
    public static void main(String[] args) {
        Sensor sensor = SensorFactoryProducer.createSensor("Company1", "SmokeSensor", 100);
        SensorSystem sensorSystem = new SensorSystem();
        sensorSystem.register((Company1SmokeSensor) sensor);
        ((Company1SmokeSensor) sensor).setNewVolume(105);
        Sensor motionSensor = SensorFactoryProducer.createSensor("Company2", "MotionSensor", 100);
        sensorSystem.register((Company2MotionSensor) motionSensor);
        ((Company2MotionSensor) motionSensor).setNewDistance(80);
        sensorSystem.notifyObservers();
//        sensorSystem.unregister((Company2MotionSensor) motionSensor);
//        sensorSystem.notifyObservers();
        HomeController homeController = new HomeController();
        homeController.setCommand(new StopAlarmCommand(new MotionAlarm()));
        homeController.buttonPressed();
        homeController.setCommand(new TurnOffSprinklerCommand(new Sprinkler()));
        homeController.buttonPressed();
    }
}
