package com.company.designPatterns;

public class Company2MotionSensor extends MotionSensor implements Observer{
    public Company2MotionSensor(int distance) {
        super(distance);
    }

    private int newDistance;

    public int getNewDistance() {
        return newDistance;
    }

    public void setNewDistance(int newDistance) {
        this.newDistance = newDistance;
    }

    @Override
    public void detect() {
        if (newDistance < super.getDistance()) {
            System.out.println("Motion detected!");
            Notification notification = new MotionNotification(super.getDistance()- newDistance);
            notification.sendNotification(new FacebookStrategy());
            notification.sendNotification(new SmsStrategy());
            HomeController homeController = new HomeController();
            homeController.setCommand(new StartAlarmCommand(new MotionAlarm()));
            homeController.buttonPressed();
        }
    }
}
