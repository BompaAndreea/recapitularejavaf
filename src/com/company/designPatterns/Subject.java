package com.company.designPatterns;

public interface Subject {
    void register(Observer sensorObserver);
    void notifyObservers();
    void unregister(Observer sensorObserver);
}
