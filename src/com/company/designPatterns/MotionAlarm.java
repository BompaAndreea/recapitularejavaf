package com.company.designPatterns;

public class MotionAlarm {

    public void startAlarm (){
        System.out.println("Alarm started!");
    }
    public void stopAlarm (){
        System.out.println("Alarm has stopped!");
    }
}
