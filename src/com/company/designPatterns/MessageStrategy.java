package com.company.designPatterns;

public interface MessageStrategy {
    public void sendMessage(String message);

}
