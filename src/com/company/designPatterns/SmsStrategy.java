package com.company.designPatterns;

public class SmsStrategy implements MessageStrategy {

    @Override
    public void sendMessage(String message) {
        System.out.println(message + "msg sent by SMS");

    }
}
