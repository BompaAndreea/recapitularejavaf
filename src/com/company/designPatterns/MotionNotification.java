package com.company.designPatterns;

public class MotionNotification extends Notification {

    private int details;

    public MotionNotification(int details) {
        this.details = details;
    }

    @Override
    public String getMessage() {
        return "Motion detected at " + details;
    }
}
