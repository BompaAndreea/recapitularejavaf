package com.company.designPatterns;

public interface Observer {
    void detect();
}
