package com.company.designPatterns;

public class Company1MotionSensor extends MotionSensor implements Observer {


    public Company1MotionSensor(int distance) {
        super(distance);
    }

    private int newDistance;

    public int getNewDistance() {
        return newDistance;
    }

    public void setNewDistance(int newDistance) {
        this.newDistance = newDistance;
    }

    @Override
    public void detect() {
        if (newDistance < super.getDistance()) {
//            System.out.println("Motion detected!");
            Logger logger = Logger.getInstance();
            logger.log("Motion detected");

            Notification notification = new MotionNotification(super.getDistance()- newDistance);
            notification.sendNotification(new FacebookStrategy());
            notification.sendNotification(new SmsStrategy());
            HomeController homeController = new HomeController();
            homeController.setCommand(new StartAlarmCommand(new MotionAlarm()));
            homeController.buttonPressed();

        }
    }
}

