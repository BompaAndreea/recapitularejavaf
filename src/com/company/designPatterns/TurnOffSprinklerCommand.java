package com.company.designPatterns;

public class TurnOffSprinklerCommand implements Command{
    Sprinkler sprinkler;

    public TurnOffSprinklerCommand(Sprinkler sprinkler) {
        this.sprinkler = sprinkler;
    }

    @Override
    public void execute() {
sprinkler.turnOFF();
    }
}
