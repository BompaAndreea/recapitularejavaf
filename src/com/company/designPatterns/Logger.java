package com.company.designPatterns;

public class Logger {

    private static final Logger instance = new Logger();

    private Logger() {

    }

    public static Logger getInstance() {
        return instance;
    }

    public void log(String s) {
        System.out.println("Logger " + s);
    }
}
