package com.company;

public class StaticOverrideMain {

    public static void main(String[] args) {

        StaticOverrideParent staticOverrideParent = new StaticOverrideChild();
        staticOverrideParent.print();

    }

}
